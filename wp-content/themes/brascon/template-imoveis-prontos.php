<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Imóveis - Prontos para morar */
get_header();
?>
<div id="primary" class="content-area content-imoveis">
	<main id="main" class="site-main">

		<section id="hero">
			<div class="container">

				<h2 class="wow fadeInRight">PRONTOS PARA MORAR</h2>

				<div class="row">

					<?php 
					$imoveis = new WP_Query( array( 
						'post_type' => 'imovel',
						'tax_query'      => array(
							array(
								'taxonomy' => 'status',
								'terms' => array('pronto-para-morar'),
								'field' => 'slug'
							)
						),
						'posts_per_page' => -1 ) );
						while ( $imoveis->have_posts() ) :  $imoveis->the_post(); ?>

							<?php if ( $imoveis->found_posts < 4 ) { ?>
								<div class="col-sm-12">
								<?php } else { ?>
									<div class="col-sm-6">
									<?php } ?>
									<div class="imovel wow fadeInUp">
										<div class="imovel-header">
											<a href="<?php echo get_permalink(); ?>" data-dot="<button role='button'><span></span></button>">
												<h3><?php the_field('slogan') ?></h3>
											</a>
											<?php 
											$images = acf_photo_gallery( 'galeria' , get_the_ID() );
											if ( is_array($images) || is_object($images) ) : ?>

												<div class="owl-container">

													<div class="navigation">
														<div class="navigation-arrows"><div class="navigation-dots"></div></div>
													</div>
													<div class="owl-gallery owl-carousel" id="imovelGallery">

														<?php foreach( $images as $image ): ?>

															<a href="<?php echo get_permalink(); ?>" data-dot="<button role='button'><span></span></button>">
																<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
															</a>

														<?php endforeach; ?>

													</div>
												</div>

											<?php endif; ?>
										</div>
										<a href="<?php echo get_permalink(); ?>">
											<div class="details">
												<div class="detail local">Miramar</div>
												<div class="detail area"><?php the_field('area') ?>m²</div>
												<div class="detail quartos"><?php the_field('quartos') ?> quarto<?php if(get_field('quartos') > 1) { echo 's'; } ?> <?php if( get_field('suites') ) { echo ' &nbsp; | &nbsp; ' . get_field('suites') . ' suíte';  } ?><?php if(get_field('suites') > 1) { echo 's'; } ?></div>
												<?php $logo = get_field('logo_branca'); if($logo) : ?>
												<img class="logo" src="<?php echo $logo ?>">
											<?php endif; ?>
										</div>
									</a>
								</div>
							</div>

							<?php wp_reset_postdata(); endwhile; ?>
							
						</div>

					</div>

				</div>
			</section>

			<?php get_template_part( 'template-parts/interesse' ) ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php
	get_footer();
