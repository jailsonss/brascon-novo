$(document).ready(function($) {

    //start wow
    new WOW().init( {
        offset:75
    });

    //start owl
    var rootClassName = 'owl-container';
    var navContainerClassName = '.navigation-arrows';
    var dotsContainerClassName = '.navigation-dots';
    $('.owl-gallery').each( function (index, item) {
        var autoplay = $(this).data('autoplay');
        var loop = false;
        if( autoplay > 0 ) { loop = true; }
        var rootIdClassName = 'owl--' + rootClassName + '--' + index;
        $(item).closest('.' + rootClassName).addClass(rootIdClassName);
        $(item).owlCarousel({
            autoplay:autoplay,
            loop:loop,
            margin:10,
            nav:true,
            items:1,
            animateOut:'fadeOut',
            navContainer: '.' + rootIdClassName + ' ' +navContainerClassName,
            dotsContainer: '.' + rootIdClassName + ' ' +dotsContainerClassName,
            dotsData: true,
        });
    });

    //menu dropdown
    $('.navbar-nav > li.dropdown').hover(function() {
        $('ul.dropdown-menu', this).stop(true, true).slideDown(400);
        $(this).addClass('open');
    }, function() {
        $('ul.dropdown-menu', this).stop(true, true).slideUp(400);
        $(this).removeClass('open');
    });

    //mobile menu toggle
    $('.menu-toggle').click(function(){
        if($('#masthead').hasClass('open')) {
            setTimeout(
              function() 
              {
                $('#main-menu').fadeOut();
            }, 500);
        } else {
            $('#main-menu').fadeIn(0);
        }
        $('#masthead').toggleClass('open');
    });

    //menu fix for imoveis
    if ( $('.single-imovel').length > 0 ) {
        $('.current_page_parent').removeClass('current_page_parent');
        $('#menu-menu-principal li:nth-child(2)').addClass('current_page_parent');
        $('input[name="imovel"]').val($('.imovel-titulo').html());
    }

    //home tabs
    $('.tab-content').hover(function(){
        $('#homeSlider').css('opacity','0');
        $('.navigation').css('opacity','0');
        $(this).find('.collapse').first().collapse('show');
        $(this).closest('.tab').removeClass('collapsed');
        var bgTarget = '.'+$(this).data('bg-target');
        $(bgTarget).css('opacity','1');
        $('.backgrounds').css('background-color','#000');
    }, function(){
        $('#homeSlider').css('opacity','1');
        $('.navigation').css('opacity','1');
        var bgTarget = '.'+$(this).data('bg-target');
        $('.tab').addClass('collapsed');
        setTimeout(
          function() 
          {
            $(bgTarget).css('opacity','0');
            $('.backgrounds').css('background-color','transparent');
            $('.collapse.show').first().collapse('hide');
        }, 300);
    });

    //input labels
    $('.form-group input').focusin(function() {
        console.log('focus');
        $(this).closest('.form-group').addClass('focus');
    });
    $('.form-group input').focusout(function() {
        // $(this).closest('.form-group').removeClass('focus');
        if (!$(this).val()) {
            $(this).closest('.form-group').removeClass('focus');
        }
    });

    //custom select
    $('.selectpicker').selectpicker({
        'noneSelectedText': 'Selecionar imóvel...',
        'countSelectedText': '{0} imóveis selecionados'
    });

    // slideDown animation
    $('.bootstrap-select').on('show.bs.dropdown', function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    $('.bootstrap-select').on('hide.bs.dropdown', function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });

    //checkbox
    $('.form-check-label').click(function(){
        $(this).toggleClass('checked');
        if( $('form-check-input').is(":checked") ) {
            $('form-check-input').attr("checked","");
        } else {
            $('form-check-input').attr("checked","checked");
        }
    });

    //file
    $("input[type='file']").change(function () {
        $('.form-group.file label').html(this.value.replace(/C:\\fakepath\\/i, ''))
    })

    //fancybox
    $("[data-fancybox]").fancybox({
        buttons: [
        "close"
        ]
    });

    //descubra accordion
    $('#accordionDescubra .collapse').on('hide.bs.collapse', function () {
        $('.collapse').eq(0).collapse('show');
    })

    $('#accordionDescubra .collapse').on('show.bs.collapse', function () {
        var current = $(this).attr('id');
        $('.'+current).addClass('active');
        $(this).closest('.card').addClass('active');
    });

    $('#accordionDescubra .collapse').on('hide.bs.collapse', function () {
        var current = $(this).attr('id');
        $('.'+current).removeClass('active');
        $(this).closest('.card').removeClass('active');
    });

    //gmaps
    if ( $('#gmaps').length ) {

        var lat = $('#gmaps').data('lat');
        var lng = $('#gmaps').data('lng');
        var imovel = '<h6>' + $('#gmaps').data('imovel') + '</h6><a href="https://www.google.com/maps/place/' + $('#gmaps').data('end') + '" target="_blank">' + $('#gmaps').data('end') + '</a>';
        var endereco = {lat: lat, lng: lng};

        var map = new google.maps.Map(
            document.getElementById('gmaps'), {
                zoom: 16,
                center: endereco,
                streetViewControl : false,
                mapTypeControl: false,
                styles: [
                {
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                    {
                        "visibility": "off"
                    }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#616161"
                    }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels",
                    "stylers": [
                    {
                        "visibility": "off"
                    }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text",
                    "stylers": [
                    {
                        "visibility": "off"
                    }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#757575"
                    }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#ffffff"
                    }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#757575"
                    }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#dadada"
                    }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#616161"
                    }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels",
                    "stylers": [
                    {
                        "visibility": "off"
                    }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                    {
                        "color": "#c9c9c9"
                    }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                    ]
                }
                ]
            });

var markers = [];
var infowindow =  new google.maps.InfoWindow();
var marker = new google.maps.Marker({position: endereco, map: map, icon: stylesheet_directory_uri + '/images/icons/icn-pin-alt.png'});
var service = new google.maps.places.PlacesService(map);
bindInfoWindow(marker, map, infowindow, imovel);

$('#mapa .locais a').each(function(){
    var type = $(this).data('slug');

    var request = {
        location: endereco,
        radius: '500',
        type: type
    };

    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, callback);

    function callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            var name = results[i].name;
            var coords = results[i].geometry.location;
            var latLng = new google.maps.LatLng(coords.lat(),coords.lng());
            var marker = new google.maps.Marker({
                position: latLng,
                icon: stylesheet_directory_uri + '/images/icons/icn-pin-' + type + '.png',
                type: type,
                name : name
                // map: map
            });
            // infoWindows[i] = new google.maps.InfoWindow();
            // console.log(marker.name);
            // google.maps.event.addListener(marker, 'click', function() {
            //     infoWindows[i].setContent(marker.name);
            //     infoWindows[i].open(map, marker);
            // });
            bindInfoWindow(marker, map, infowindow, marker.name);
            markers.push(marker);
            // setMapOnAll(type,map);

        }
    }
}
// addMarker(haightAshbury);
});

function bindInfoWindow(marker, map, infowindow, name) {
    marker.addListener('click', function() {
        infowindow.setContent(name);
        infowindow.open(map, this);
    });
}

// Adds a marker to the map and push to the array.
function addMarker(location) {
    var marker = new google.maps.Marker({
      position: location,
      map: map
  });
    markers.push(marker);
}

      // Sets the map on all markers in the array.
      function setMapOnAll(type,map) {
        for (var i = 0; i < markers.length; i++) {
            if(markers[i].type == type) {
              markers[i].setMap(map);
          }
      }
  }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
    }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
    }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

}

$('#mapa .locais a').click(function(e){
    e.preventDefault();
    $('#mapa .locais .current').removeClass('current');
    var type = $(this).data('slug');

    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
  }

  if (type!="all") {
    $(this).addClass('current');
    for (var i = 0; i < markers.length; i++) {
        if(markers[i].type != type) {
            markers[i].setMap(null);
        }
    }
}

});

//contact forms
var wpcf7Elm = document.querySelector( 'body' );

wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
    $('.modal').modal('hide');
    $('#obrigado').modal('show');
}, false );

  //open filters on mobile
  $('.open-filters').click(function(){
    $('#sidebar').addClass('open');
});

  $('.close-filters').click(function(){
    $('#sidebar').removeClass('open');
});

  //slider
  if ( $('#quartosSlider').length ) {

      var quartosLoaded = false;
      var quartosSlider = document.getElementById('quartosSlider');
      noUiSlider.create(quartosSlider, {
        start: [$(quartosSlider).data('min-value'), $(quartosSlider).data('max-value')],
        connect: true,
        step: 1,
        range: {
            'min': $(quartosSlider).data('min-value'),
            'max': $(quartosSlider).data('max-value')
        },
        format: wNumb({
            decimals: 0
        })
    });

      var quartosSliderValue = document.getElementById('quartosSliderValue');

      quartosSlider.noUiSlider.on('update', function (values) {
        quartosSliderValue.innerHTML = values.join(' - ');
        $('#loop-options').data('quartos-min',values[0]);
        $('#loop-options').data('quartos-max',values[1]);
        if (areaLoaded && quartosLoaded) {
            $('#loop-options').data('page','1');
            filterImoveis();
        }
    });

  }

  if ( $('#areaSliderValue').length ) {

    var areaLoaded = false;
    var areaSlider = document.getElementById('areaSlider');
    noUiSlider.create(areaSlider, {
        start: [$(areaSlider).data('min-value'), $(areaSlider).data('max-value')],
        connect: true,
        range: {
            'min': $(areaSlider).data('min-value'),
            'max': $(areaSlider).data('max-value')
        },
        step: 1,
        format: wNumb({
            decimals: 0
        })
    });

    var areaSliderValue = document.getElementById('areaSliderValue');

    areaSlider.noUiSlider.on('update', function (values) {
        // alert(areaLoaded);
        areaSliderValue.innerHTML = values.join(' - ');
        $('#loop-options').data('area-min',values[0]);
        $('#loop-options').data('area-max',values[1]);
        if (areaLoaded && quartosLoaded) {
            $('#loop-options').data('page','1');
            filterImoveis();
        }
        if (quartosLoaded) {
            areaLoaded = true;
            filterImoveis();
        }
        quartosLoaded = true;
    });

}

  //ajax filters for imoveis
  $('#filter-ordem a').click(function(e){
    e.preventDefault();
    $('#loop-options').data('page','1');
    if( !$(this).hasClass('current') ) {
        $('#filter-ordem .current').removeClass('current');
        $(this).addClass('current');
        $('#loop-options').data('ordem',$(this).data('ordem'));
        filterImoveis(1);
    } else {
        $(this).removeClass('current');
        $('#loop-options').data('ordem','');
        filterImoveis(1);
    }
});

  $('#filter-intencao a.filter').click(function(e){
    e.preventDefault();
    $('#loop-options').data('page','1');
    if( !$(this).hasClass('current') ) {
        $('#filter-intencao .current').removeClass('current');
        $(this).addClass('current');
        $('#loop-options').data('intencao',$(this).data('intencao'));
        filterImoveis(1);
    } else {
        $(this).removeClass('current');
        $('#loop-options').data('intencao','');
        filterImoveis(1);
    }
});

  $('#filter-tipo a').click(function(e){
    e.preventDefault();
    $('#loop-options').data('page','1');
    if( !$(this).hasClass('current') ) {
        $('#filter-tipo .current').removeClass('current');
        $(this).addClass('current');
        $('#loop-options').data('tipo',$(this).data('tipo'));
        filterImoveis(1);
    } else {
        $(this).removeClass('current');
        $('#loop-options').data('tipo','');
        filterImoveis(1);
    }
});

  $('#filter-status a').click(function(e){
    e.preventDefault();
    $('#loop-options').data('page','1');
    if( !$(this).hasClass('current') ) {
        $('#filter-status .current').removeClass('current');
        $(this).addClass('current');
        $('#loop-options').data('status',$(this).data('status'));
        filterImoveis(1);
    } else {
        $(this).removeClass('current');
        $('#loop-options').data('status','');
        filterImoveis(1);
    }
    // var status = "";
    // if( !$(this).hasClass('current') ) {
    //     $(this).addClass('current');
    //     $('#filter-status a.current').each(function(){
    //         status += $(this).data('status')+',';
    //     });
    //     $('#loop-options').data('status',status);
    //     filterImoveis(1);
    // } else {
    //     $(this).removeClass('current');
    //     $('#filter-status a.current').each(function(){
    //         status += $(this).data('status')+',';
    //     });
    //     $('#loop-options').data('status',status);
    //     filterImoveis(1);
    // }
});

  $('#filter-cidade a').click(function(e){
    e.preventDefault();
    $('#loop-options').data('page','1');
    var cidade = "";
    if( !$(this).hasClass('current') ) {
        $(this).addClass('current');
        $('#filter-cidade a.current').each(function(){
            cidade += $(this).data('cidade')+',';
        });
        $('#loop-options').data('cidade',cidade);
        filterImoveis(1);
    } else {
        $(this).removeClass('current');
        $('#filter-cidade a.current').each(function(){
            cidade += $(this).data('cidade')+',';
        });
        $('#loop-options').data('cidade',cidade);
        filterImoveis(1);
    }
});

  $('#filter-bairro a').click(function(e){
    e.preventDefault();
    $('#loop-options').data('page','1');
    var bairro = "";
    if( !$(this).hasClass('current') ) {
        $(this).addClass('current');
        $('#filter-bairro a.current').each(function(){
            bairro += $(this).data('bairro')+',';
        });
        $('#loop-options').data('bairro',bairro);
        filterImoveis();
    } else {
        $(this).removeClass('current');
        $('#filter-bairro a.current').each(function(){
            bairro += $(this).data('bairro')+',';
        });
        $('#loop-options').data('bairro',bairro);
        filterImoveis();
    }
});

    //ajax filters for posts
    $('#loop-filters a').click(function(e){
        e.preventDefault();
        $('#loop-options').data('cat',$(this).data('cat-id'));
        $('#loop-options').data('page',1);
        $("#loop-filters a").removeClass("current");
        $(this).addClass("current"); 
        filterPosts();
    });

    // $('.loop-content').on('click','a',function(e){
    //     e.preventDefault();
    //     $('#loop-options').data('page',$(this).data('page'));
    //     filterPosts();
    // });

    $(document).on('click','.content-blog #navigation a',function(e){
      e.preventDefault();
      $('#loop-options').data('page',$(this).data('page'));
      filterPosts();
  });

    $(document).on('click','.content-imoveis-list #navigation a',function(e){
      e.preventDefault();
      // alert($(this).data('page'));
      $('#loop-options').data('page',$(this).data('page'));
      filterImoveis();
  });

});

function filterPosts() {
    // $('#loop-options').data('page',page);
    $('#loop-content').addClass('loading');
    $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "load-post-filter", page: $('#loop-options').data('page'), cat: $('#loop-options').data('cat') },
        success: function(response) {
            $("#loop-content").html(response);
            setTimeout(
              function() 
              {
                $('#loop-content').removeClass('loading');
            }, 300);
            return false;
        }
    });
}

function filterImoveis() {
    // e.preventDefault();
    // $('#loop-options').data('page',page);
    $('#loop-content').addClass('loading');
    $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: { 
            "action": "load-imoveis-filter",
            page: $('#loop-options').data('page'),
            ordem: $('#loop-options').data('ordem'),
            intencao: $('#loop-options').data('intencao'),
            tipo: $('#loop-options').data('tipo'),
            status: $('#loop-options').data('status'),
            cidade: $('#loop-options').data('cidade'),
            bairro: $('#loop-options').data('bairro'),
            quartos_min: $('#loop-options').data('quartos-min'), 
            quartos_max: $('#loop-options').data('quartos-max'), 
            area_min: $('#loop-options').data('area-min'), 
            area_max: $('#loop-options').data('area-max') 
        },
        success: function(response) {
            $("#loop-content").html(response);
            setTimeout(
              function() 
              {
                $('#loop-content').removeClass('loading');
            }, 300);
            return false;
        }
    });
}