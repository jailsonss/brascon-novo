<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Descubra */
get_header();
?>
<div id="primary" class="content-area content-descubra">
	<main id="main" class="site-main">

		<section id="hero" class="animated fadeIn">

			<div class="hero-bg">
				<div class="bg-item collapseNatureza active" style="background-image: url(<?php the_field('imagem_box_1') ?>)"></div>
				<div class="bg-item collapseQualidade" style="background-image: url(<?php the_field('imagem_box_2') ?>)"></div>
				<div class="bg-item collapseCusto" style="background-image: url(<?php the_field('imagem_box_3') ?>)"></div>
			</div>
			
			<div class="container">

				<h2 class="animated fadeInRight">descubra<br>joÃo pessoa</h2>

				<div class="accordion animated fadeInUp" id="accordionDescubra">
					<div class="card active">
						<div class="card-header" id="headingOne">
							<h3 class="mb-0">
								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseNatureza" aria-expanded="true" aria-controls="collapseNatureza">
									<?php the_field('titulo_box_1') ?>
								</button>
							</h3>
						</div>

						<div id="collapseNatureza" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionDescubra">
							<div class="card-body">
								<?php the_field('texto_box_1') ?>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingTwo">
							<h3 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseQualidade" aria-expanded="false" aria-controls="collapseQualidade">
									<?php the_field('titulo_box_2') ?>
								</button>
							</h3>
						</div>
						<div id="collapseQualidade" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionDescubra">
							<div class="card-body">
								<?php the_field('texto_box_2') ?>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingThree">
							<h3 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseCusto" aria-expanded="false" aria-controls="collapseCusto">
									<?php the_field('titulo_box_3') ?>
								</button>
							</h3>
						</div>
						<div id="collapseCusto" class="collapse" aria-labelledby="headingThree" data-parent="#accordionDescubra">
							<div class="card-body">
								<?php the_field('texto_box_3') ?>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>


		<?php get_template_part( 'template-parts/newsletter' ) ?>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
