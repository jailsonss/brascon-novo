<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

<div id="primary" class="content-area content-blog invert">
	<main id="main" class="site-main">

		<div class="container">

			<header class="page-header animated fadeInUp">
				<h1><?php single_post_title(); ?></h1>
				<form action="<?php echo home_url( '/' ); ?>" method="get" class="dark">
					<div class="form-group">
						<label for="nome">Buscar</label>
						<input type="text" name="s" class="form-control" id="search" placeholder="Buscar" value="<?php the_search_query(); ?>">
						<input type="image" alt="Search" src="<?php echo get_template_directory_uri() ?>/images/icons/icn-search.png">
					</div>
				</form>
			</header>

			<?php 

			$featureds = get_posts( array( 
				'post_type' => 'post',
				'tag' => 'destaque',
				'posts_per_page' => 1 ) );

				foreach ( $featureds as $featured ) : ?>

					<section id="blog-hero" class="animated fadeInUp">
						<a href="<?php echo get_permalink($featured->ID) ?>">
							<div class="row no-gutters">
								<div class="col-lg-7">
									<div class="hero-cover img-cover" style="background-image: url(<?php echo get_the_post_thumbnail_url($featured->ID)?>);"></div>
								</div>
								<div class="col-lg-5">
									<div class="hero-body">
										<h2><?php echo $featured->post_title ?></h2>
										<p class="meta"><?php echo get_the_date('',$featured->ID) ?></p>
										<p class="excerpt"><?php echo get_the_excerpt($featured->ID) ?></p>
										<p class="link">Ver matéria</p>
									</div>
								</div>
							</div>
						</a>
					</section>

					<?php
				endforeach;

				$loop = new WP_Query( array( 
					'post_type' => 'post',
					'post__not_in' => array($featured->ID)
				)
			);

			$categories = get_categories(); ?>

			<ul id="loop-filters">
				<li><a class="todos current" data-cat-id="" href="#"><h4>Todos</h4></a></li>
				<?php foreach ( $categories as $cat ) { ?>
					<li id="<?php echo $cat->term_id; ?>"><a class="<?php echo $cat->slug; ?>" data-cat-id="<?php echo $cat->term_id; ?>" href="#"><h4><?php echo $cat->name; ?>s</h4></a></li>

				<?php } ?>
			</ul>

			<!-- <div id="category-post-content"></div> -->

			<div id="loop-options" data-page="<?php echo $page ?>" data-cat=""></div>

			<div id="loop-content">

				<?php
				if ( have_posts() ) :

					echo '<div class="d-flex flex-wrap">';

					while ( $loop -> have_posts() ) : $loop -> the_post();

						get_template_part( 'template-parts/article-loop' );

					endwhile;

					echo '</div>';

					numeric_posts_nav();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

			</div>

		</div>

	</main><!-- #main -->

	<?php get_template_part( 'template-parts/newsletter' ) ?>

</div><!-- #primary -->

<?php
get_footer();
