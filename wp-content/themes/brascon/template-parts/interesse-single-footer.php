<section id="contact-widget" class="single">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="contact-form">
					<h3 class="wow fadeInRight">Tem interesse no <span class="imovel-titulo"><?php echo get_the_title() ?></span>?</h3>
					<?php echo do_shortcode('[contact-form-7 id="497" title="Interesse - Single Footer"]'); ?>
				</div>
			</div>
			<div class="col-sm-12 d-sm-none">
				<div class="form-img wow fadeInUpBig"></div>
			</div>
		</div>
	</div>
	<a href="#" class="qualitare"></a>
</section>