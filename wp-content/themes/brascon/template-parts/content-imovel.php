<div id="primary" class="content-area content-single content-single-imovel">
	<main id="main" class="site-main">

		<div class="modal fade" id="interesse" tabindex="-1" role="dialog" aria-labelledby="interesseLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="337" title="Interesse - Single"]'); ?>
				</div>
			</div>
		</div>

		<div class="modal fade" id="tabela" tabindex="-1" role="dialog" aria-labelledby="tabelaLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="348" title="Tabela de preço"]'); ?>
				</div>
			</div>
		</div>

		<article id="<?php the_ID() ?>" class="post-<?php the_ID() ?>">


			<section id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);">

				<div class="img-mobile d-sm-none" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);"></div>

				<div class="hero-content">

					<div class="container">
						<?php 
						$video = get_field('video');
						if ( $video ) { 
							echo '<a data-fancybox href="'.$video.'" class="video-play"><span>Veja o vídeo</span></a>';
						}?>
						<div class="row no-gutters">
							<div class="col-md-8">
								<h3><?php the_field('slogan') ?></h3>			
							</div>
							<div class="col-md-4">
								<img src="<?php the_field('logo') ?>" class="logo img-fluid">
							</div>
						</div>
					</div>

				</div>

			</section>

			<section id="content">

				<div class="article-body">

					<div class="container">

						<div class="row">
							<div class="col-md-6">
								<div class="sobre">
									<img src="<?php the_field('imagem_do_imovel') ?>" class="img-imovel img-fluid d-sm-none">
									<p><?php the_field('sobre') ?></p>
									<a href="#" class="button black interesse" data-toggle="modal" data-target="#interesse">TENHO INTERESSE</a>
									<a href="#" class="button black precos" data-toggle="modal" data-target="#tabela">TABELA DE PREÇOS</a>
								</div>
							</div>
							<div class="col-md-6">
								<?php $tag = get_the_terms(get_the_ID(),'status'); ?>
								<div class="img-imovel">
									<?php if ( ! empty( $tag ) ) { echo '<div class="tag">'.$tag[0]->name.'</div>';  } ?>
									<img src="<?php the_field('imagem_do_imovel') ?>" class=" img-fluid hidden-sm">
								</div>
							</div>
						</div>

						<div class="row detalhes">
							<div class="col-md-4">
								<h4><?php the_field('tipo_1') ?></h4>
								<ul>
									<li class="area"><?php the_field('area') ?>m²</li>
									<li class="quarto"><?php the_field('quartos') ?> quarto<?php if( get_field('quartos') > 1 ) { echo 's'; } ?></li>
									<?php for ($i=1; $i < 16 ; $i++) { ?>
										<?php if (get_field('titulo_'.$i)): ?>
											<li><span class="icon" style="background-image: url(<?php the_field('icone_'.$i) ?>);"></span><?php the_field('titulo_'.$i) ?></li>
										<?php endif ?>
									<?php } ?>
									<!-- <li class="sala"><?php the_field('salas') ?> sala<?php if( get_field('salas') > 1 ) { echo 's'; } ?></li>
									<li class="banheiro"><?php the_field('banheiros') ?> banheiro<?php if( get_field('banheiros') > 1 ) { echo 's'; } ?></li>
									<li class="cozinha"><?php the_field('cozinhas') ?> cozinha<?php if( get_field('cozinhas') > 1 ) { echo 's'; } ?></li>
									<li class="servico"><?php the_field('areas_de_servico') ?> área<?php if( get_field('areas_de_servico') > 1 ) { echo 's'; } ?> de serviço</li>
									<li class="vaga"><?php the_field('vagas') ?> vaga<?php if( get_field('vagas') > 1 ) { echo 's'; } ?></li> -->
								</ul>
							</div>
							<?php if( get_field('cobertura') ) : ?>
								<div class="col-md-4">
									<h4><?php the_field('tipo_2') ?></h4>
									<ul>
										<?php for ($i=1; $i < 16 ; $i++) { ?>
											<?php if (get_field('titulo_2_'.$i)): ?>
												<li><span class="icon" style="background-image: url(<?php the_field('icone_2_'.$i) ?>);"></span><?php the_field('titulo_2_'.$i) ?></li>
											<?php endif ?>
										<?php } ?>
										<!-- <li class="sala"><?php the_field('salas_2') ?> sala<?php if( get_field('salas_2') > 1 ) { echo 's'; } ?></li>
										<li class="quarto"><?php the_field('quartos_2') ?> quarto<?php if( get_field('quartos_2') > 1 ) { echo 's'; } ?></li>
										<li class="area"><?php the_field('area_2') ?>m²</li>
										<li class="banheiro"><?php the_field('banheiros_2') ?> banheiro<?php if( get_field('banheiros_2') > 1 ) { echo 's'; } ?></li>
										<li class="cozinha"><?php the_field('cozinhas_2') ?> cozinha<?php if( get_field('cozinhas_2') > 1 ) { echo 's'; } ?></li>
										<li class="servico"><?php the_field('areas_de_servico_2') ?> área<?php if( get_field('areas_de_servico_2') > 1 ) { echo 's'; } ?> de serviço</li>
										<li class="vaga"><?php the_field('vagas_2') ?> vaga<?php if( get_field('vagas_2') > 1 ) { echo 's'; } ?></li> -->
									</ul>
								</div>
							<?php endif ?>
							<div class="col-md-4">
								<h4>Outras características</h4>
								<div class="outras">
									<ul>
										<?php for ($i=1; $i < 16 ; $i++) { ?>
											<?php if (get_field('titulo_3_'.$i)): ?>
												<li><?php the_field('titulo_3_'.$i) ?></li>
											<?php endif ?>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>

					</div>

				</div>

			</section>

			<?php 
			$images = acf_photo_gallery( 'galeria' , get_the_ID() );
			if ( !empty($images) ) : ?>
				<section id="galeria">

					<div class="owl-container">
						<div class="container">
							<h2>Galeria</h2>
						</div>
						<div class="navigation">
							<div class="navigation-arrows"><div class="navigation-dots"></div></div>
						</div>
						<div class="owl-gallery owl-carousel" id="imovelGallery">

							<?php foreach( $images as $image ): ?>

								<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="galeria" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
									<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
								</a>

							<?php endforeach; ?>

						</div>
					</div>

				</section>
			<?php endif; ?>

			<?php 
			$images = acf_photo_gallery( 'plantas' , get_the_ID() );
			if ( !empty($images) ) : ?>
				<section id="plantas">

					<div class="container">
						<h2>Plantas</h2>
						<div class="owl-container">
							<div class="owl-gallery owl-carousel" id="plantasGallery">

								<?php foreach( $images as $image ): ?>

									<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="plantas" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
										<img class="thumb" src="<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>">
									</a>

								<?php endforeach; ?>

							</div>
							<div class="navigation dark">
								<div class="navigation-arrows"><div class="navigation-dots"></div></div>
							</div>
						</div>
					</div>

				</section>
			<?php endif; ?>

			<?php if( get_field('tour_virtual') ) : ?>

				<section id="tour" style="background-image: url(<?php the_field('imagem_tour') ?>);">

					<div class="container">
						<div class="row">
							<div class="col-sm-6 col-md-5">
								<div class="content wow fadeInUp">
									<h2>Tour virtual</h2>
									<p><?php the_field('tour_virtual') ?></p>
									<a href="#" class="button interesse" data-toggle="modal" data-target="#interesse">TENHO INTERESSE</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-7">
								<a data-fancybox href="<?php the_field('video_tour_virtual') ?>" class="video-play white"><span>TOUR VIRTUAL</span></a>
							</div>
						</div>
					</div>

				</section>

			<?php endif; ?>

			<?php 
			$obras = get_posts( array( 
				'post_type' => 'obra',
				// 'meta_query'	=> array(
				// 	array(
				// 		'key'	 	=> 'imovel',
				// 		'value'	  	=> get_the_ID(),
				// 		'type'    => 'numeric',
				// 		'compare' => 'BETWEEN'
				// 	)
				// ),
				'meta_key'			=> 'imovel',
				'meta_value' 		=> get_the_ID(),
				'posts_per_page' => 1 ) );

				foreach ( $obras as $obra ) : ?>

					<?php 
					$images = acf_photo_gallery( 'galeria' , $obra->ID );
					if ( is_array($images) || is_object($images) ) : ?>
						<section id="obras">

							<div class="owl-container">

								<div class="owl-gallery owl-carousel" id="obrasGallery">

									<?php foreach( $images as $image ): ?>

										<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>"></div>

									<?php endforeach; ?>

								</div>

								<div class="container">
									<div class="row">
										<div class="col-md-7">
											<div class="navigation">
												<div class="navigation-arrows"><div class="navigation-dots"></div></div>
											</div>
										</div>
										<div class="col-md-5">
											<div class="content wow fadeInUp">
												<h2>Acompanhamento da obra</h2>
												<p>Projeto <?php the_field('projeto',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('projeto',$obra->ID) ?>%"></span></div>
												<p>Fundação <?php the_field('fundacao',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('fundacao',$obra->ID) ?>%"></span></div>
												<p>Estrutura <?php the_field('estrutura',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('estrutura',$obra->ID) ?>%"></span></div>
												<p>Alvenaria <?php the_field('alvenaria',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('alvenaria',$obra->ID) ?>%"></span></div>
												<p>Revestimento externo <?php the_field('revestimento_externo',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('revestimento_externo',$obra->ID) ?>%"></span></div>
												<p>Revestimento interno <?php the_field('revestimento_interno',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('revestimento_interno',$obra->ID) ?>%"></span></div>
												<p>Fôrros <?php the_field('forros',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('forros',$obra->ID) ?>%"></span></div>
												<p>Bancadas, louças e metais <?php the_field('bancadas',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('bancadas',$obra->ID) ?>%"></span></div>
												<p>Paisagismo <?php the_field('paisagismo',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('paisagismo',$obra->ID) ?>%"></span></div>
												<p>Acabamento <?php the_field('acabamento',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('acabamento',$obra->ID) ?>%"></span></div>
												<p>Limpeza <?php the_field('limpeza',$obra->ID) ?>%</p>
												<div class="status"><span style="width: <?php the_field('limpeza',$obra->ID) ?>%"></span></div>
												<p class="total">Total <?php the_field('total',$obra->ID) ?>%</p>
												<div class="status total"><span style="width: <?php the_field('total',$obra->ID) ?>%"></span></div>
												<form class="row d-none">
													<div class="col-md-12">
														<h5>Ver meses anteriores</h5>
													</div>
													<div class="col-md-5">
														<div class="form-group">
															<select class="selectpicker form-control show-menu-arrow" title="Ano" data-selected-text-format="count">
																<?php 
																$obras_relacionadas = get_posts( array( 
																	'post_type' => 'obra',
																	'meta_query'	=> array(
																		array(
																			'key'	 	=> 'imovel',
																			'value'	  	=> get_the_ID(),
																			'type'    => 'numeric',
																			'compare' => 'BETWEEN'
																		)
																	),
																	'meta_key'			=> 'ano',
																	'orderby'			=> 'meta_value_num meta_value',
																	'order'				=> 'DESC',
																	'posts_per_page' => -1 ) );
																foreach ( $obras_relacionadas as $obra_relacionadas ) :
																	?>
																	<option value=""><?php the_field('ano',$obra_relacionadas->ID) ?></option>
																<?php endforeach; ?>
															</select>
														</div>
													</div>
													<div class="col-md-7">
														<div class="form-group">
															<select id="obrasMes" class="selectpicker form-control show-menu-arrow" title="Mês" data-selected-text-format="count">
																<!-- <option>Janeiro</option> -->
																<!-- <option>Fevereiro</option> -->
															</select>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>

							</div>

						</section>
					<?php endif; endforeach; ?>

					<section id="mapa">
						<?php 
						$locais = get_posts( array( 
							'post_type' => 'local',
							'posts_per_page' => -1 ) 
					);
						$endereco = get_field('endereco');
						?>

						<div id="gmaps" data-imovel="<?php the_title(); ?>" data-end="<?php echo esc_attr($endereco['address']); ?>" data-lat="<?php echo esc_attr($endereco['lat']); ?>" data-lng="<?php echo esc_attr($endereco['lng']); ?>"></div>
						<div class="container">
							<div class="row justify-content-end">
								<div class="col-md-3">
									<div class="content">
										<h4>Você encontra no bairro</h4>
										<p class="caption">Clique nas categorias abaixo para exibi-las no mapa:</p>
										<ul class="locais">
											<?php foreach ($locais as $local ) : ?>
												<li><a href="#" data-slug="<?php echo $local->slug ?>" class="<?php echo $local->slug ?>"><?php echo get_the_title($local->ID) ?></a></li>
											<?php endforeach ?>
											<li><a href="#" data-slug="all" class="all">Exibir tudo</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</section>


					</article>

				</main><!-- #main -->
			</div><!-- #primary -->

			<?php get_template_part( 'template-parts/interesse-single-footer' ) ?>