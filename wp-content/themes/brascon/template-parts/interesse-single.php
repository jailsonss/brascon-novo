<section id="contact-widget" class="single">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="contact-form">
					<h3 class="wow fadeInRight">Tem interesse no <?php echo get_the_title() ?>?</h3>
					<?php echo do_shortcode('[contact-form-7 id="337" title="Interesse - Single"]'); ?>
				</div>
			</div>
			<div class="col-sm-12 d-sm-none">
				<div class="form-img wow fadeInUpBig"></div>
			</div>
		</div>
	</div>
	<a href="#" class="qualitare"></a>
</section>