<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

	<div id="primary" class="content-area error-404 not-found">
		<main id="main" class="site-main">

			<section class="message-404 animated fadeIn">
				<h1>404</h1>
				<h4>Página não encontrada</h4>
				<p>Parece que você acessou um link que não existe mais.</p>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="button white">IR pARA iNício</a>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
