<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_brascon');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WPCF7_AUTOP', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[C`7FWS+mZ1x>a,I/;c!V{u.gx|.XQuoBM=*Xc)F&tJnJSdqk}roKhP#*V88)aL!');
define('SECURE_AUTH_KEY',  '==N7>&[BKFeeEv<>%.rz,|-QbW!E&@?ib;jel.2 ?C8rf.~/`zB>_`uhR0nt*z21');
define('LOGGED_IN_KEY',    ',:It3YbEO<p->Sd4^J}/qAP#O;rv`ITog/`,!Wt0p&&J`4>knvB$ZU=98Bfd_L=r');
define('NONCE_KEY',        'Ui028[1:F?F>F%&_)?Amk.[i6sV8#^$rWp~>&FXu,|.DJRoj8;Y&t6cEg3b*-K+(');
define('AUTH_SALT',        'joGP<^0nKD5y:2zsRIb6@CoMW]?UzMHX{~D*iLqHH?Kl:~ z{Fyh,w?s7)MX2k%n');
define('SECURE_AUTH_SALT', 'nCz/am9V3`.f82mO9ZQ1@Cu[j=d8aYpK:)l2!2uYolLeR)sD&;K[:sK**c++2z.j');
define('LOGGED_IN_SALT',   'NkbFrA]06ju)vvOKD4ED%LByd:Sv<{C32>]nfCi_p^ZeX)PpK6[`Mz67MKsZ~pkR');
define('NONCE_SALT',       'o{yTK7iCK;kBxv3ZY12PwS>&Xndy_3Cqz:Dt(_1;Sku:.D|D7A>MNkP`VdSwR!xb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
